# Tally

**Student Gradebook**

Tally is a basic, text based, gradebook for teachers that uses the `cmd.Cmd`
framework. It tracks students (with class, average and scores) in a class and
their grades. It assigns scores on tests and assignments to students and
figures theiraverage and grade for the class.

