from setuptools import setup


setup(
      name="Tally",
      version="0.1.0",
      packages=["tally"],
      url="https://bitbucket.org/protocypher/ma-tally",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A school gradebook application for teachers."
)

